package test;

import static org.junit.Assert.*;

import java.awt.event.KeyEvent;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import Bots.EasyBot;
import Bots.HardBot;
import Bots.MediumBot;
import FileManagement.Load;
import FileManagement.Save;
import FileManagement.UpdateStatistic;
import Game.Babu;
import Game.Field;
import Game.Special_Functions;
import Game.WinCheck;
import Graphics.Map;

public class test {
	
	Field f;
	WinCheck check;
	HardBot hardbot;
	Map map;
	
	@Before
	public void setUp() {
		f= new Field();
		check = new WinCheck();
		hardbot = new HardBot();
		map = new Map(check, null);
		f.getField().get(1).set(1, Babu.X);
		f.getField().get(3).set(3, Babu.X);
		f.getField().get(7).set(0, Babu.X);
		f.getField().get(5).set(5, Babu.O);
		f.getField().get(5).set(6, Babu.O);
		f.getField().get(5).set(7, Babu.O);
		f.getField().get(5).set(8, Babu.O);
		f.getField().get(5).set(9, Babu.O);
	}
	

	@Test
	public void testEasy() {
		int[] pos = EasyBot.step(3, 3, f);
		Assert.assertNotEquals(1, pos[0]);
		Assert.assertNotEquals(5, pos[1]);
	}
	
	@Test
	public void testMedium() {
		int[] pos = MediumBot.step(3, 3, check.getWincheck(), f);
		Assert.assertEquals(4, pos[0]);
		Assert.assertEquals(4, pos[1]);
	}
	
	@Test
	public void testHard() {
		int[] VH = map.getVerticalHorizontal();
		int[] pos = hardbot.step(3, 3, check.getWincheck(), f, VH[0], VH[1]);
		Assert.assertEquals(4, pos[0]);
		Assert.assertEquals(4, pos[1]);
	}
	
	@Test
	public void testfieldExtends() {
		f.fieldExtends();
		Assert.assertEquals(11, f.getFieldWidth());
		Assert.assertEquals(Babu.X, f.getField().get(7).get(1));
		Assert.assertEquals(null, f.getField().get(7).get(0));
	}
	@Test
	public void testWinCheck() {
		check = check.winCheck(6, 7, 5, Babu.O, f);
		Assert.assertEquals(false, check.getWin());
		check = check.winCheck(5, 7, 5, Babu.O, f);
		Assert.assertEquals(true, check.getWin());
	}
	
	@Test
	public void testLoadStatistics() {
		int[] statistics = UpdateStatistic.loadStatistics();
		Assert.assertEquals(0, statistics[2]);
	}
	
	@Test
	public void testLoadFFileNull() {
		f = Load.loadF();
		Assert.assertEquals(10, f.getFieldHeight());
		Assert.assertEquals(null, f.getField().get(0).get(7));
	}
	
	@Test
	public void testLoadWhichFileNull() {
		int random = Special_Functions.getRandomNumber(2, 2);
		Assert.assertEquals(2, random);
	}
	
	@Test
	public void testCheckDirection() {
		int[] direction = f.fieldExtends();
		Assert.assertEquals(0, direction[0]);
		Assert.assertEquals(1, direction[1]);
		Assert.assertEquals(0, direction[2]);
		Assert.assertEquals(1, direction[3]);
	}
	@Test
	public void testSaveLoad() {
		Save.save(f, map.getMap(), "Hard");
		String which = Load.loadWhich();
		Assert.assertEquals("Hard", Load.loadWhich());
		f = Load.loadF();
		Assert.assertEquals(10, f.getFieldHeight());
		Assert.assertEquals(Babu.O, f.getField().get(5).get(6));
		Assert.assertEquals(Babu.X, f.getField().get(3).get(3));
	}
}
