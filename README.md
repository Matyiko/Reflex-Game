# Infinitely expanding Tic-Tac-Toe

## A special Tic-Tac-Toe created in Java with OpenGL where you can play against bots on an infinitely extending map!

This project is a recreation of an of my older homework with better algorithms and stronger bots.

The simple rules of the game that:
* You start on a 10x10 map that extends everytime someone puts his mark on the border of the map.
* You always play with X.
* Win by having 5 connected X in any directions
* Lose by having opponent has 5 connected O in any directions.
* You can move around the map by the WASD keys.

The game contains three different bots:

* easy
* normal
* hard

You can play against them and see a statistic that shows how many times did you defeated them.
The easy and normal bots are that intresting, however the hard bot can be a real challenge.

## How to install the project!

The easiest way to do it:

1. clone the project
2. open the folder in vsCode
3. install the Extension Pack for Java
4. run the Main.java file

## Environment

    Java
## Version

    2.3.8
