package Game;

import java.util.ArrayList;

/**
 * A Wincheket tartalmazó osztály, ez értékeli a győzelmet és segíti a botokat.
 */
public class WinCheck {
    private boolean win;
    private ArrayList<Babu> row;
    private String merre;
    private int n;
    private int[][] colorable;
    private int max, dbOther;

    /**
     * A Wincheck kontruktora, beállítja a tagváltozók értékét alapállapotba, a legtöbb esetben ez 0/null.
     */
    public WinCheck() {
        this.win = false;
        this.merre = null;
        this.n = 0;
        this.row = new ArrayList<Babu>(6);
        for(int i = 0; i < 6; i++){
                row.add(null);
        }
        colorable = new int[5][2];
        for(int i = 0; i < 5; i++) {
            this.colorable[i][0] = 0;
            this.colorable[i][1] = 0;
        }
        this.max = 0;
        this.dbOther = 0;
    }

    /**
     * A dbOther getter függvénye.
     * @return Visszadja a dbOther értékét.
     */
    public int getDbOther(){
        return this.dbOther;
    }

    /**
     * A max getter függvénye.
     * @return Visszadja a max értékét.
     */
    public int getMax(){
        return this.max;
    }

    /**
     * 0-ra állítja a row, merre, b, dbOther és a db értékét.
     * @return 0-át vissza paraméterként a db 0-ázása gyanánt.
     */
    private int reset (){
        for(int i = 0; i < 6; i++){
            this.row.set(i, null);
        }
        this.merre = null;
        this.n = 0;
        this.dbOther = 0;
        return 0;
    }

    /**
     * A Wincheck getter függvénye.
     * @return Visszadja a Wincheket.
     */
    public WinCheck getWincheck(){
        return this;
    }

    /**
     * A Colorable getter függvénye.
     * @return Visszadja a Colorable-t.
     */
    public int[][] getColorable(){
        return this.colorable;
    }

    /**
     * A Win getter függvénye.
     * @return Visszadja a Win-t.
     */
    public boolean getWin() {
        return this.win;
    }

    /**
     * A Merre getter függvénye.
     * @return Visszadja a Merre-t.
     */
    public String getMerre() {
        return (this.merre);
    }

    /**
     * A Row getter függvénye.
     * @return Visszadja a Row-t.
     */
    public ArrayList<Babu> getRow() {
        return (ArrayList<Babu>) this.row.clone();
    }

    /**
     * Az N getter függvénye.
     * @return Visszadja a N-t.
     */
    public int getN() {
        return this.n;
    }

    /**
     * Az N setter függvnénye;
     * @param n Egy int;
     */
    public void setN(int n){ this.n = n;}

    /**
     * A merre setter függvnénye;
     * @param merre Egy irány;
     */
    public void setMerre(String merre){
        this.merre = merre;
    }


    /**
     * A row setter függvnénye;
     * @param row Egy sor;
     */
    public void setRow(ArrayList<Babu> row){ this.row = row;}

    /**
     * Ha s = 5-re hívjuk, akkor megmondja, hogy nyert-e valaki, ha s kisebb 5-re,
     * akkor megszámolja, hogy mennyi a legtöbb babu valamelyik irányba
     * mehívja, az adjustment függvényt.
     * @param h A mező magasság kordinátája.
     * @param w A mező szélesség kordinátája.
     * @param s A vizsgálálandó mezők száma.
     * @param babu A babu, amire vizsgálunk.
     * @param f A Field, amit vizsgálunk.
     * @return Visszaadd egy Wincheket.
     */
    public WinCheck winCheck(int h, int w, int s, Babu babu, Field f){
        this.max = 0;
        int db = 0;
        /**
         * sor check
         */
        for (int k = -s + 1; k < 1; k++) {
            for (int i = 0; i < s; i++){
                if (w + k + i < f.getFieldWidth() && w + k + i >= 0 && f.getField().get(h).get(w + k + i) == babu) {
                    db++;
                    colorable[i][0] = h;
                    colorable[i][1] = w + k + i;

                }
                else if (w + k + i < f.getFieldWidth() && w + k + i >= 0 && f.getField().get(h).get(w + k + i) != babu
                        && w + k + i < f.getFieldWidth() && w + k + i >= 0 && f.getField().get(h).get(w + k + i) != null) {
                    db = 0;
                    break;
                }
            }
            if(max < db) max = db;
            if(db == 5) {
                win = true;
                return this;
            }
            db = 0;
        }
        /**
         * oszlop check
         */
        for (int k = -s + 1; k < 1; k++) {
            for (int i = 0; i < s; i++){
                if (h + k + i < f.getFieldHeight() && h + k + i >= 0 && f.getField().get(h + k + i).get(w) == babu) {
                    db++;
                    colorable[i][0] = h + k + i;
                    colorable[i][1] = w;
                }
                else if (h + k + i < f.getFieldHeight() && h + k + i >= 0 && f.getField().get(h + k + i).get(w) != babu
                        && h + k + i < f.getFieldHeight() && h + k + i >= 0 && f.getField().get(h + k + i).get(w) != null) {
                    db = 0;
                    break;
                }
            }
            if(max < db) max = db;
            if(db == 5) {
                win = true;
                return this;
            }
            db = 0;
        }
        /**
         * Bal fentről jobbra le check
         */
        for (int k = -s + 1; k < 1; k++) {
            for (int i = 0; i < s; i++){
                if (w + k + i < f.getFieldWidth() && w + k + i >= 0
                        && h + k + i < f.getFieldHeight() && h + k + i >= 0
                        && f.getField().get(h + k + i).get(w + k + i) == babu) {
                    db++;
                    colorable[i][0] = h + k + i;
                    colorable[i][1] = w + k + i;
                }
                else if (w + k + i < f.getFieldWidth() && w + k + i >= 0
                        && h + k + i < f.getFieldHeight() && h + k + i >= 0
                        && f.getField().get(h + k + i).get(w + k + i) != babu && f.getField().get(h + k + i).get(w + k + i) != null) {
                    db = 0;
                    break;
                }
            }
            if(max < db) max = db;
            if(db == 5) {
                win = true;
                return this;
            }
            db = 0;
        }
        /**
         * Bal lentről jobbra fel check
         */
        for (int k = -s + 1; k < 1; k++) {
            for (int i = 0; i < s; i++){
                if (w + k + i < f.getFieldWidth() && w + k + i >= 0
                        && h - k - i < f.getFieldHeight() && h - k - i >= 0
                        && f.getField().get(h - k - i).get(w + k + i) == babu) {
                    db++;
                    colorable[i][0] = h - k - i;
                    colorable[i][1] = w + k + i;
                }
                else if (w + k + i < f.getFieldWidth() && w + k + i >= 0
                        && h - k - i < f.getFieldHeight() && h - k - i >= 0
                        && f.getField().get(h - k - i).get(w + k + i) != babu && f.getField().get(h - k - i).get(w + k + i) != null) {
                    db = 0;
                    break;
                }
            }
            if(max < db) max = db;
            if(db == 5) {
                win = true;
                return this;
            }
            db = 0;
        }
        if(s < 5) {
            adjustment(max, h, w, s, babu, f);
        }
        return this;
    }

    /**
     * Meghatározza melyik irányba van a legtöbb babu és elteszi őket egy listába,
     * ez segíti a botokat, hogy jól tudjanak reagálni a lépésekre.
     * @param max Max darab babu a legtöbb valamyelyik irányba.
     * @param h A mező magasság kordinátája.
     * @param w A mező szélesség kordinátája.
     * @param s A vizsgálálandó mezők száma.
     * @param babu A babu, amire vizsgálunk.
     * @param f A Field, amit vizsgálunk.
     * @return Visszaadd egy Wincheket.
     */
    private WinCheck adjustment(int max, int h, int w, int s, Babu babu, Field f){
        int db = 0;
        /**
         * sor check
         */
        for (int k = -s + 1; k < 1; k++) {
            for (int i = 0; i < s; i++) {
                if (w + k + i < f.getFieldWidth() && w + k + i >= 0) {
                    if(f.getField().get(h).get(w + k + i) == babu){
                        this.merre = "sor";
                        this.n = -k;
                        db++;
                    }
                    else if(f.getField().get(h).get(w + k + i) != null && f.getField().get(h).get(w + k + i) != babu){
                        db = 0;
                        break;
                    }
                    this.row.set(i + 1, f.getField().get(h).get(w + k + i));
                }
            }
            if (db == max) {
                if (w + k + -1 >= 0) {
                    this.row.set(0, f.getField().get(h).get(w + k - 1));
                    if(f.getField().get(h).get(w + k - 1) != null && f.getField().get(h).get(w + k - 1) != babu){
                        dbOther++;
                    }
                } else this.row.set(0, Babu.O);
                if (w + k + 4 < f.getFieldWidth()) {
                    this.row.set(5, f.getField().get(h).get(w + k + 4));
                    if(f.getField().get(h).get(w + k + 4) != null && f.getField().get(h).get(w + k + 4) != babu){
                        dbOther++;
                    }
                } else this.row.set(5, Babu.O);
                return this;
            } else {
                db = reset();
            }
        }
        /**
         * oszlop check
         */
        for (int k = -s + 1; k < 1; k++) {
            for (int i = 0; i < s; i++) {
                if (h + k + i < f.getFieldHeight() && h + k + i >= 0) {
                    if (f.getField().get(h + k + i).get(w) == babu) {
                        this.merre = "oszlop";
                        this.n = -k;
                        db++;
                    }
                    else if (f.getField().get(h + k + i).get(w) != null && f.getField().get(h + k + i).get(w) != babu) {
                        db = 0;
                        break;
                    }
                    this.row.set(i + 1, f.getField().get(h + k + i).get(w));
                }
            }
            if (db == max) {
                if (h + k + -1 >= 0) {
                    this.row.set(0, f.getField().get(h + k - 1).get(w));
                    if(f.getField().get(h + k - 1).get(w) != null && f.getField().get(h + k - 1).get(w) != babu){
                        dbOther++;
                    }
                } else this.row.set(0, Babu.O);
                if (h + k + 4 < f.getFieldHeight()) {
                    this.row.set(5, f.getField().get(h + k + 4).get(w));
                    if(f.getField().get(h + k + 4).get(w) != null && f.getField().get(h + k + 4).get(w) != babu){
                        dbOther++;
                    }
                } else this.row.set(5, Babu.O);
                return this;
            } else {
                db = reset();
            }
        }
        /**
         * Bal fentről jobbra le check
         */
        for (int k = -s + 1; k < 1; k++) {
            for (int i = 0; i < s; i++) {
                if (w + k + i < f.getFieldWidth() && w + k + i >= 0
                        && h + k + i < f.getFieldHeight() && h + k + i >= 0) {
                    if (f.getField().get(h + k + i).get(w + k + i) == babu) {
                        this.merre = "fentrol_atlo";
                        this.n = -k;
                        db++;
                    }
                    else if (f.getField().get(h + k + i).get(w + k + i) != null && f.getField().get(h + k + i).get(w + k + i) != babu) {
                        db = 0;
                        break;
                    }
                    this.row.set(i + 1, f.getField().get(h + k + i).get(w + k + i));
                }
            }
            if (db == max) {
                if (h + k + -1 >= 0 && w + k + -1 >= 0) {
                    this.row.set(0, f.getField().get(h + k - 1).get(w + k - 1));
                    if(f.getField().get(h + k - 1).get(w + k - 1) != null && f.getField().get(h + k - 1).get(w + k - 1) != babu){
                        dbOther++;
                    }
                } else this.row.set(0, Babu.O);
                if (h + k + 4 < f.getFieldHeight() && w + k + 4 < f.getFieldWidth()) {
                    this.row.set(5, f.getField().get(h + k + 4).get(w + k + 4));
                    if(f.getField().get(h + k + 4).get(w + k + 4) != null && f.getField().get(h + k + 4).get(w + k + 4) != babu){
                        dbOther++;
                    }
                } else this.row.set(5, Babu.O);
                return this;
            } else {
                db = reset();
            }
        }
        /**
         * Bal lentről jobbra fel check
         */
        for (int k = -s + 1; k < 1; k++) {
            for (int i = 0; i < s; i++) {
                if (w + k + i < f.getFieldWidth() && w + k + i >= 0
                        && h - k - i < f.getFieldHeight() && h - k - i >= 0) {
                    if (f.getField().get(h - k - i).get(w + k + i) == babu) {
                        this.merre = "lentrol_atlo";
                        this.n = -k;
                        db++;
                    }
                    else if (f.getField().get(h - k - i).get(w + k + i) != null && f.getField().get(h - k - i).get(w + k + i) != babu) {
                        db = 0;
                        break;
                    }
                    this.row.set(i + 1, f.getField().get(h - k - i).get(w + k + i));
                }
            }
            if (db == max) {
                if (h - k + 1 < f.getFieldHeight() && w + k + -1 >= 0) {
                    this.row.set(0, f.getField().get(h - k + 1).get(w + k - 1));
                    if(f.getField().get(h - k + 1).get(w + k - 1) != null && f.getField().get(h - k + 1).get(w + k - 1) != babu){
                        dbOther++;
                    }
                } else this.row.set(0, Babu.O);
                if (h - k - 4 >= 0 && w + k + 4 < f.getFieldWidth()) {
                    this.row.set(5, f.getField().get(h - k - 4).get(w + k + 4));
                    if(f.getField().get(h - k - 4).get(w + k + 4) != null && f.getField().get(h - k - 4).get(w + k + 4) != babu){
                        dbOther++;
                    }
                } else this.row.set(5, Babu.O);
                return this;
            } else {
                db = reset();
            }
        }
        return this;
    }
}
