package Game;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * A pályát tartalmazó osztály. A pálya létrehozásával, bővülésével, kirajzolásával foglalkozik.
 */
public class Field implements Serializable {

    private ArrayList<ArrayList<Babu>> field;
    private int height;
    private int width;
    private int[] direction;

    /**
     * A pálya konstruktora.
     * létrehoz egy pályát, beállíttja a magasságát, létrehozza a listákat feltölti őket 0-ákkal.
     */
    public Field() {
        this.height = 10;
        this.width = 10;
        this.field = new ArrayList<ArrayList<Babu>>(height);
        for(int i = 0; i < height; i++) {
            field.add(new ArrayList<>(width));
        }
        for(int i = 0; i < height; i++){
            for(int j = 0; j < width; j++) {
                field.get(i).add(null);
            }
        }
        this.direction = new int[4];
    }

    /**
     * A field getter függvénye.
     * @return Visszadja a field értékét.
     */
    public ArrayList<ArrayList<Babu>> getField() {
        return this.field;
    }

    /**
     * A height getter függvénye.
     * @return visszadja a pálya magasságát.
     */
    public int getFieldHeight(){
        return this.height;
    }

    /**
     * A width getter függvénye.
     * @return Visszadja a pálya szélességét.
     */
    public int getFieldWidth(){
        return this.width;
    }

    /**
     * Ha van Babu a pálya szélén, akkor abba az irányba bővíti a pályát és áthelyezi az elemeket az eltolásnak megfelelően.
     * Megvizsgálja melyik irányba bővült a pálya.
     * @return A bővülés iránya.
     */
    public int[] fieldExtends() {
        int left = 0;
        int up = 0;
        int right = 0;
        int down = 0;

        for (int i = 0; i < height; i++) {
            if(field.get(i).get(0) != null){
                left = 1;
            }
        }
        for (int i = 0; i < height; i++) {
            if(field.get(i).get(width - 1) != null){
                right = 1;
            }
        }
        for (int i = 0; i < width; i++) {
            if(field.get(0).get(i) != null){
                up = 1;
            }
        }
        for (int i = 0; i < width; i++) {
            if(field.get(height - 1).get(i) != null){
                down = 1;
            }
        }

        direction[0] = up;
        direction[1] = left;
        direction[2] = down;
        direction[3] = right;

        if(up != 0 || down != 0 || left!= 0 || right != 0) {
            int h = 0;
            if (up == 1 || down == 1) h = 1;
            int w = 0;
            if (left == 1 || right == 1) w = 1;
            height += h;
            width += w;

            field = Special_Functions.cloneArray(field, left, up, right, down);
        }
        return direction;
    }

    /**
     * Kirajzolja a pálya jelenlegi állását a kozonlra .
     */
    public void draw(){
        for(int i=0;i<height;i++){
            for(int j=0;j<width;j++){
                if(field.get(i).get(j) != null) System.out.print(field.get(i).get(j));
                else System.out.print("-");
            }
            System.out.println();
        }
    }
}
