package Game;

import java.util.ArrayList;

/**
 * A különlegesebb függvényeket tartalmazza, amelyek statikusak és érdemesebb volt külön class-ba tenni őket.
 */
public class Special_Functions {
    /**
     * Kap egy pályát és irányokat paraméterként, ezek alapján létrehoz egy új nagyobb listát,
     * amit feltölt a régi lista elemeivel a paramétereknek megfelelően eltolva és visszaadja.
     * @param src A pálya, amit ki kell bővíteni.
     * @param left A balra bővítés mértéke.
     * @param up A felfele bővítés mértéke.
     * @param right A jobbra bővítés mérték.
     * @param down A lefele bővítés mértéke.
     * @return Visszaadja az új kibővített pályát.
     */
    public static ArrayList<ArrayList<Babu>> cloneArray(ArrayList<ArrayList<Babu>> src, int left, int up, int right, int down) {
        int h = 0;
        if(up == 1 || down == 1) h = 1;
        int w = 0;
        if(left == 1 || right == 1) w = 1;
        int length = src.size();
        ArrayList<ArrayList<Babu>> target = new ArrayList<>(length + h);
        for(int i=0; i < length + h; i++) {
            target.add(new ArrayList(src.get(0).size() + w));
        }
        for(int i = 0; i < length + h; i++){
            for(int j = 0; j < src.get(0).size() + w; j++) {
                target.get(i).add(null);
            }
        }

        for (int i = 0; i < length; i++) {
            for(int j = 0; j < src.get(0).size(); j++) {
                target.get(i + up).set(j + left, src.get(i).get(j));
            }
        }
        return target;
    }

    /**
     * Kap két számot paraméterként, és visszaad egy random számot a kapott paraméterek között.
     * @param min Az alsó határ.
     * @param max A felső határ.
     * @return
     */
    public static int getRandomNumber(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }
}
