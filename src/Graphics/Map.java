package Graphics;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.Serializable;

import Bots.EasyBot;
import Bots.HardBot;
import Bots.MediumBot;
import FileManagement.UpdateStatistic;
import Game.Babu;
import Game.Field;
import FileManagement.Save;
import Game.WinCheck;


/**
 * A pályát és a gombokat kirajzoló grafikai osztály.
 */
public class Map extends JPanel implements ItemListener, ActionListener, KeyListener, Serializable {
    private JButton[][] b;
    private JPanel map, buttons;
    private JButton save, save2;
    private JButton[] wasd;

    private Field f;
    private int verticalHorizontal[];
    private int[] direction;
    private Babu[][] view;
    private int[] offset;

    private int[] statistics;
    private String who;
    HardBot hardBot;

    /**
     * Az F setter függvénye.
     * @param f A Field
     */
    public void setF(Field f){
        this.f = f;
    }

    /**
     * A Map getter függvénye.
     * @return visszadja a Map-ot.
     */
    public Map getMap(){
        return this;
    }
    
    /**
     * A VerticalHorizontal getter függvénye.
     * @return visszadja a VerticalHorizontal-t.
     */
    public int[] getVerticalHorizontal(){
        return this.verticalHorizontal;
    }

    /**
     * A VerticalHorizontal setter függvénye.
     * @param VH int tömb.
     */
    public void setVerticalHorizontal(int[] VH){
        this.verticalHorizontal = VH;
    }

    /**
     * A Map kontruktora létrohozza a pályát és a gombokat.
     * @param check Egy Wincheck.
     * @param which Az adott bot, ami ellen játszunk.
     */
    public Map(WinCheck check, String which){
        this.setFocusTraversalKeysEnabled(false);
        map = new JPanel();
        buttons = new JPanel();
        buttons.setLayout(new BoxLayout(buttons, BoxLayout.PAGE_AXIS));
        save = new JButton("Save");
        save.setFocusable(false);
        save2 = new JButton("Save");
        save2.setFocusable(false);
        wasd = new JButton[4];
        wasd[0] = new JButton("W");
        wasd[1] = new JButton("A");
        wasd[2] = new JButton("S");
        wasd[3] = new JButton("D");
        for(int i = 0; i< 4; i++){
            wasd[i].setPreferredSize(new Dimension(50, 30));
            wasd[i].setFocusable(false);
        }
        save.setPreferredSize(new Dimension(70, 30));
        save2.setPreferredSize(new Dimension(70, 30));
        Box box = new Box(BoxLayout.X_AXIS);
        box.add(Box.createHorizontalGlue());
        box.add(save);
        box.add(Box.createHorizontalGlue());
        box.add(wasd[1]);
        box.add(wasd[2]);
        box.add(wasd[3]);
        box.add(Box.createHorizontalGlue());
        box.add(save2);
        box.add(Box.createHorizontalGlue());

        Box box2 = new Box(BoxLayout.X_AXIS);
        box2.add(Box.createHorizontalGlue());
        box2.add(wasd[0]);
        box2.add(Box.createHorizontalGlue());

        buttons.add(box, BoxLayout.X_AXIS);
        buttons.add(box2, BoxLayout.X_AXIS);
        b = new JButton[10][10];
        f = new Field();
        map.setLayout(new GridLayout(10, 10));
        this.verticalHorizontal = new int[4];
        this.verticalHorizontal[0] = 0;
        this.verticalHorizontal[1] = 0;
        direction = new int[4];
        this.view = new Babu[10][10];
        who = null;
        statistics = UpdateStatistic.loadStatistics();
        hardBot = new HardBot();

        for(int i = 0;i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                b[i][j] = new JButton();
                b[i][j].setFocusable(false);
                map.add(b[i][j]);
                int tmpI = i;
                int tmpJ = j;
                b[i][j].addActionListener(new ActionListener() {
                    /**
                     * Ha rakni akarunk és a pályán egy gombra kattintunk, akkor elképezi,
                     * hogy hova kell kerülnie a háttérben a pályán a babu-nak.
                     * @param e Egy ActionEvent
                     */
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if(who == null) {
                            int x = tmpI + verticalHorizontal[0] - offset[0];
                            int y = tmpJ + verticalHorizontal[1] - offset[1];
                            if (f.getField().get(x).get(y) == null) {
                                who = getView(check.getWincheck(), x, y, which);
                            }
                        }
                    }
                });
            }
        }
        offset = new int[2];
        offset[0] = 0;
        offset[1] = 0;

        save.addActionListener(new ActionListener() {
            /**
             * Ez ment, ha a save gombra kattintunk.
             * @param e Egy ActionEvent
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                Save.save(f, getMap(), which);
            }
        });
        save2.addActionListener(new ActionListener() {
            /**
             * Ez ment, ha a save2 gombra kattintunk.
             * @param e Egy ActionEvent
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                Save.save(f, getMap(), which);
            }
        });
        this.setLayout(new BorderLayout());
        this.setPreferredSize(new Dimension(950, 940));


        this.add(map, BorderLayout.CENTER);
        this.add(buttons, BorderLayout.SOUTH);

        this.addKeyListener(this);
    }

    /**
     * Atadjuk neki hova szeretnénk rakni, a függvény kiszámolja,
     * hogy ez a pályán hol van és kirajzolja, ezután meghívja a botot, hogy rakjon és megint kirajzolja a pályát, emelett frissíti a statisztikát, ha győzünk.
     * @param check Egy Wincheck.
     * @param x Az adott gomb magasság kordinátája a pályára átszámolva.
     * @param y Az adott gomb szélesség kordinátája a pályára átszámolva
     * @param which Az adott bot.
     * @return Visszaadja ki nyert.
     */
    public String getView(WinCheck check, int x, int y, String which){
        f.getField().get(x).set(y, Babu.X);
        /**
         * Jtékos lép.
         */
        check.winCheck(x, y, 5, Babu.X, f);
        drawView();
        if(check.getWin() == true){
            int[][] colorable = check.getColorable();
            for(int i = 0; i < 5; i++){
                int buttonI = colorable[i][0] - verticalHorizontal[0] + offset[0];
                int buttonJ = colorable[i][1] - verticalHorizontal[1] + offset[1];
                if(buttonI >= 0 && buttonI < 10 && buttonJ >= 0 && buttonJ < 10) {
                    b[buttonI][buttonJ].setBackground(Color.green);
                }
            }
            if(which.equals("Easy")){
                statistics[0]++;
            }
            else if(which.equals("Medium")){
                statistics[1]++;
            }
            else if(which.equals("Hard")){
                statistics[2]++;
            }
            UpdateStatistic.updateStatistics(statistics);
            return "Win";
        }
        direction = f.fieldExtends();

        if(direction[0] == 1) verticalHorizontal[0]++;
        if(direction[1] == 1) verticalHorizontal[1]++;
        if(direction[2] == 1) verticalHorizontal[2]--;
        if(direction[3] == 1) verticalHorizontal[3]--;
        f.draw();
        /**
         * Bot lép.
         */
        int pos[] = new int[2];
        if(which.equals("Easy")){
            pos = EasyBot.step(x, y, f);
        }
        else if(which.equals("Medium")) {
            pos = MediumBot.step(x, y, check.getWincheck(), f);
        }
        else if(which.equals("Hard")) {
            pos = hardBot.step(x, y, check.getWincheck() ,f, verticalHorizontal[0], verticalHorizontal[1]);
        }
        check.winCheck(pos[0], pos[1], 5, Babu.O, f);

        drawView();

        if(check.getWin() == true){
            int[][] colorable = check.getColorable();
            for(int i = 0; i < 5; i++){
                int buttonI = colorable[i][0] - verticalHorizontal[0] + offset[0];
                int buttonJ = colorable[i][1] - verticalHorizontal[1] + offset[1];
                if(buttonI >= 0 && buttonI < 10 && buttonJ >= 0 && buttonJ < 10) {
                    b[buttonI][buttonJ].setBackground(Color.red);
                }
            }
            return "Lose";
        }

        direction = f.fieldExtends();
        if(direction[0] == 1) verticalHorizontal[0]++;
        if(direction[1] == 1) verticalHorizontal[1]++;
        if(direction[2] == 1) verticalHorizontal[2]--;
        if(direction[3] == 1) verticalHorizontal[3]--;
        f.draw();

        return null;
    }

    /**
     * Kirajzolja a játék jelenlegi állásából, azt a részt, amit mi látunk.
     */
    public void drawView(){
        for(int i = 0; i < 10; i++){
            for(int j = 0; j < 10; j++){
                this.view[i][j] = f.getField().get(i + verticalHorizontal[0] - offset[0]).get(j + verticalHorizontal[1] - offset[1]);
            }
        }

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (view[i][j] == Babu.X) {
                    b[i][j].setText("X");
                } else if (view[i][j] == Babu.O) {
                    b[i][j].setText("O");
                }
                else{
                    b[i][j].setText(null);
                }
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    /**
     * Ennek a függvény segítségével tudunk wasd-el mozogni a pályán.
     * @param e Egy KeyEvent
     */
    @Override
    public void keyPressed(KeyEvent e) {
        if(who == null) {
            if (e.getKeyChar() == 'w') {
                if (verticalHorizontal[0] > offset[0]) {
                    offset[0]++;
                }
            } else if (e.getKeyChar() == 's') {
                if (verticalHorizontal[2] < offset[0]) {
                    offset[0]--;
                }
            } else if (e.getKeyChar() == 'a') {
                if (verticalHorizontal[1] > offset[1]) {
                    offset[1]++;
                }
            } else if (e.getKeyChar() == 'd') {
                if (verticalHorizontal[3] < offset[1]) {
                    offset[1]--;
                }
            }
            drawView();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
}
