package Graphics;

import FileManagement.UpdateStatistic;

import javax.swing.*;
import java.awt.*;

public class Statistics extends JPanel {
    private int[] statistics;

    private JTextField easy, medium, hard;

    public Statistics(){
        this.setLayout(new BorderLayout());
        if(UpdateStatistic.loadStatistics() != null) {
            this.statistics = UpdateStatistic.loadStatistics();
        }
        else {
            statistics = new int[3];
            for(int i = 0; i < 3; i++){
                statistics[i] = 0;
            }
        }
        easy = new JTextField();
        medium = new JTextField();
        hard = new JTextField();
        easy.setText("Easy: " + String.valueOf(statistics[0]));
        medium.setText("Medium: " + String.valueOf(statistics[1]));
        hard.setText("Hard: " + String.valueOf(statistics[2]));
        Font font = new Font("SansSerif", Font.BOLD, 50);
        easy.setFont(font);
        medium.setFont(font);
        hard.setFont(font);
        easy.setBackground(Color.lightGray);
        medium.setBackground(Color.lightGray);
        hard.setBackground(Color.lightGray);

        Box box = new Box(BoxLayout.Y_AXIS);
        box.add(Box.createVerticalGlue());
        box.add(easy);
        box.add(Box.createVerticalGlue());
        box.add(medium);
        box.add(Box.createVerticalGlue());
        box.add(hard);
        box.add(Box.createVerticalGlue());

        this.add(box);
    }
}
