package Graphics;

import FileManagement.Load;
import FileManagement.UpdateStatistic;
import Game.WinCheck;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Ez az osztály a legfelsőbb grafikai osztály, ez tartalmazza a menüt és erre kerül rá a pálya is.
 */
public class Menu extends JFrame{
    private JMenu menu;
    private JMenuBar menubar;
    private JMenuItem load, easy, medium, hard, statistic;
    private Map map;
    private JPanel stat;

    /**
     * A Menu kontruktora létrehozza a menut.
     */
    public Menu(){
        super("Game");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(950, 1000);
        setResizable(false);
        setFocusable(true);
        this.setFocusTraversalKeysEnabled(false);

        menubar = new JMenuBar();
        menu = new JMenu("Menu");
        load = new JMenuItem("Continue");
        easy = new JMenuItem("Easy");
        medium = new JMenuItem("Medium");
        hard = new JMenuItem("Hard");
        statistic = new JMenuItem("Statistic");
        menu.add(load);
        menu.add(easy);
        menu.add(medium);
        menu.add(hard);
        menu.add(statistic);
        menubar.add(menu);
        add(menubar, BorderLayout.NORTH);
        stat = new Statistics();
        add(stat);

        load.addActionListener(new ActionListener() {
            /**
             * Betölti az elmentett játékot;
             * @param e Egy ActionEvent.
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("load");
                WinCheck check = new WinCheck();
                if(map != null) {
                    remove(map);
                }
                map = new Map(check, Load.loadWhich());
                map.setVerticalHorizontal(Load.loadVH());
                map.setF(Load.loadF());
                add(map, BorderLayout.SOUTH);
                setVisible(true);
                map.requestFocusInWindow();
                map.setF(Load.loadF());
                map.drawView();
            }
        });

        easy.addActionListener(new ActionListener() {
            /**
             * Elindít egy könnyű bot elleni játékot.
             * @param e Egy ActionEvent
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("easy");
                WinCheck check = new WinCheck();
                if(stat != null){
                    remove(stat);
                }
                if(map != null) {
                    remove(map);
                }
                map = new Map(check, "Easy");
                add(map, BorderLayout.SOUTH);
                setVisible(true);
                map.requestFocusInWindow();
            }
        });

        medium.addActionListener(new ActionListener() {
            /**
             * Elindít egy közepes bot elleni játékot.
             * @param e Egy ActionEvent
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("medium");
                WinCheck check = new WinCheck();
                if(stat != null){
                    remove(stat);
                }
                if(map != null) {
                    remove(map);
                }
                map = new Map(check, "Medium");
                add(map, BorderLayout.SOUTH);
                setVisible(true);
                map.requestFocusInWindow();
            }
        });

        hard.addActionListener(new ActionListener() {
            /**
             * Elindít egy nehéz bot elleni játékot.
             * @param e Egy ActionEvent
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                WinCheck check = new WinCheck();
                if(stat != null){
                    remove(stat);
                }
                if(map != null) {
                    remove(map);
                }
                map = new Map(check, "Hard");
                add(map, BorderLayout.SOUTH);
                setVisible(true);
                map.requestFocusInWindow();
            }
        });

        statistic.addActionListener(new ActionListener() {
            /**
             * Betölti a statisztikát.
             * @param e Egy ActionEvent
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                if(stat != null){
                    remove(stat);
                }
                if(map != null) {
                    remove(map);
                }
                stat = new Statistics();
                add(stat);
                setVisible(true);
            }
        });

    }
}
