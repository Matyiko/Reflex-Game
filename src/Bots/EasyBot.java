package Bots;

import Game.Babu;
import Game.Field;
import Game.Special_Functions;

import java.util.ArrayList;

/**
 * A legkönnyebb botot tartalmazó osztály.
 */
public class EasyBot{

    /**
     * A legkönnyebb bot lépés függvénye.
     * @param h A játékos előző lépésének magasság kordinátája.
     * @param w A játékos előző lépésének szélesség kordinátája.
     * @param f A pálya.
     * @return A bot lépéséne kordinátái.
     */
    public static int[] step(int h, int w, Field f) {
        int pos[] = new int[2];

        int ranI;
        int ranJ;
        int db = 0;
        while(db < 15) {
            ranI = Special_Functions.getRandomNumber(2, -2);
            ranJ = Special_Functions.getRandomNumber(2, -2);
            if(h + ranI >= 0 && h + ranI < f.getFieldHeight() && w + ranJ >= 0 && w + ranJ < f.getFieldWidth() && f.getField().get(h + ranI).get(w + ranJ) == null){
                f.getField().get(h + ranI).set(w + ranJ, Babu.O);
                pos[0] = h + ranI;
                pos[1] = w + ranJ;
                return pos;
            }
            db++;
        }
        while(true){
             ranI = Special_Functions.getRandomNumber(0, f.getFieldHeight());
             ranJ = Special_Functions.getRandomNumber(0, f.getFieldWidth());
            if(f.getField().get(ranI).get(ranJ) == null){
                f.getField().get(ranI).set(ranJ, Babu.O);
                pos[0] = ranI;
                pos[1] = ranJ;
                return pos;
            }
        }
    }
}
