package Bots;

import Game.Babu;
import Game.Field;
import Game.WinCheck;

import java.util.ArrayList;

/**
 * A legnehezebb botot tartalmazó osztály.
 */
public class HardBot {
    private ArrayList<Integer> lastPosH;
    private ArrayList<Integer> lastPosW;
    private int index;
    private int horz, vert;

    /**
     * A nehézbot konstruktora beállítja tagváltozók értékét.
     */
    public HardBot(){
        lastPosH = new ArrayList<>();
        lastPosW = new ArrayList<>();
        horz = 0;
        vert = 0;
    }

    /**
     * A legnehezebb bot lépés függvénye.
     * @param h A játékos előző lépésének magasság kordinátája.
     * @param w A játékos előző lépésének szélesség kordinátája.
     * @param f A pálya.
     * @param c Egy Wincheck.
     * @param horizontal A magasságbeli eltolódás a pályán.
     * @param vertical A vízszintes eltolódás a pályán.
     * @return A bot lépésének kordinátái.
     */
    public int[] step(int h, int w, WinCheck c, Field f, int horizontal, int vertical){
        int tmpH = horizontal - horz;
        int tmpV = vertical - vert;
        if(tmpH > 0 || tmpV > 0){
            for(int i = 0; i < lastPosH.size(); i++){
                lastPosH.set(i, lastPosH.get(i) + tmpH);
                lastPosW.set(i, lastPosW.get(i) + tmpV);
            }
            horz = horizontal;
            vert = vertical;
        }
        lastPosH.add(h);
        lastPosW.add(w);
        int db;
        int dbOther;
        int max = -5;
        int[] pos = new int[2];
        int n = 0;
        String merre = null;
        ArrayList<Babu> row = null;

        while (true) {
            for (int i = 0; i < lastPosH.size(); i++) {
                c = c.winCheck(lastPosH.get(i), lastPosW.get(i), 4, f.getField().get(lastPosH.get(i)).get(lastPosW.get(i)), f);
                db = c.getMax();
                dbOther = c.getDbOther();
                if (db - dbOther > max) {
                    max = db - dbOther;
                    index = i;
                    n = c.getN();
                    merre = c.getMerre();
                    row = c.getRow();
                    for (int m = 0; m < 6; m++) {
                    }
                }
            }
            boolean go = false;
            c.setN(n);
            c.setMerre(merre);
            c.setRow(row);
            for (int i = 0; i < 6; i++) {
                if (c.getRow().get(i) == null) {
                    go = true;
                }
            }
            if (go) {
                MediumBot.helpBotUpgrade(lastPosH.get(index), lastPosW.get(index), c, f, pos);
                lastPosH.add(pos[0]);
                lastPosW.add(pos[1]);
                return pos;
            }
            else {
                lastPosH.remove(index);
                lastPosW.remove(index);
                max = 0;
            }
        }
    }
}
