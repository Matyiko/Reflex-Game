package Bots;

import Game.Field;
import Game.Babu;
import Game.Special_Functions;
import Game.WinCheck;

import java.util.ArrayList;

/**
 * A közepes botot tartalmazó osztály.
 */
public class MediumBot {
    /**
     * A közepes bot lépés függvénye.
     * @param h A játékos előző lépésének magasság kordinátája.
     * @param w A játékos előző lépésének szélesség kordinátája.
     * @param f A pálya.
     * @param c Egy Wincheck.
     * @return
     */
    public static int[] step(int h, int w, WinCheck c, Field f){
        int[] pos = new int[2];
        WinCheck tmp = c.winCheck(h, w, 4, f.getField().get(h).get(w), f);
        for(int i=0;i<6;i++){
            System.out.println(tmp.getRow().get(i));
        }
        System.out.println("n:" + tmp.getN());

        boolean go = false;
        for(int i = 0; i < 6; i++){
            if(tmp.getRow().get(i) == null){
               go = true;
            }
        }
        if(go) {
            MediumBot.helpBotUpgrade(h, w, tmp, f, pos);
        }
        if(!go){
            boolean done = false;
            while (!done) {
                int randomX = Special_Functions.getRandomNumber(0, f.getFieldHeight());
                int randomY = Special_Functions.getRandomNumber(0, f.getFieldHeight());
                if (f.getField().get(randomX).get(randomY) == null) {
                    f.getField().get(randomX).set(randomY, Babu.O);
                    done = true;
                }
            }
        }
        return pos;
    }

    /**
     * Ez a függvény segíti a botot abban, hogy a Wincheckben tárolt sor alapján eldöntse, hova kell raknia.
     * @param h A játékos előző lépésének magasság kordinátája.
     * @param w A játékos előző lépésének szélesség kordinátája.
     * @param tmp A Wincheck.
     * @param f A pálya.
     * @param pos A tömb, amit felkell tölteni a rakott babu poziciójával.
     */
    public static void helpBotUpgrade(int h, int w, WinCheck tmp, Field f, int[] pos){
        boolean success = false;
        boolean backward;
        int k = 1;

        if(tmp.getN() == 0 || tmp.getN() == 1){
            backward = false;
        }
        else backward = true;
        while (!success){
            if(tmp.getRow().get(k + tmp.getN() + 1) == null){
                if(tmp.getMerre().equals("sor")){
                    f.getField().get(h).set(w + k, Babu.O);
                    pos[0] = h;
                    pos[1] = w + k;
                    System.out.println("k:" + k);
                    System.out.println("ide rakok-> h:" + h + " w:" + (w+k));
                    success = true;

                }
                else if(tmp.getMerre().equals("oszlop")){
                    f.getField().get(h + k).set(w, Babu.O);
                    pos[0] = h + k;
                    pos[1] = w;
                    System.out.println("k:" + k);
                    System.out.println("ide rakok-> h:" + (h+k) + " w:" + (w));
                    success = true;
                }
                else if(tmp.getMerre().equals("fentrol_atlo")){
                    f.getField().get(h + k).set(w + k, Babu.O);
                    pos[0] = h + k;
                    pos[1] = w + k;
                    System.out.println("k:" + k);
                    System.out.println("ide rakok-> h:" + (h+k) + " w:" + (w+k));
                    success = true;
                }
                else if(tmp.getMerre().equals("lentrol_atlo")){
                    f.getField().get(h - k).set(w + k, Babu.O);
                    pos[0] = h - k;
                    pos[1] = w + k;
                    System.out.println("k:" + k);
                    System.out.println("ide rakok-> h:" + (h-k) + " w:" + (w+k));
                    success = true;
                }
            }
            if(k + tmp.getN() == 4) {
                backward = true;
            }
            if(k + tmp.getN() == -1){
                backward = false;
            }
            if(backward){
                k--;
            }
            else k++;
        }
    }

}
