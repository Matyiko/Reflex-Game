package FileManagement;

import Game.Babu;
import Game.Field;
import Graphics.Map;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;


/**
 * Ez az osztály tölti be az elmentett játék állását a fájlokból, ha nincs mentés, akkor indít egy új játékot a  legkönyebb bot ellen.
 */
public class Load {

    /**
     * Betölti egy fájból a mentett játék Field-jét.
     * @return Visszaad egy Fieldet.
     */
    public static Field loadF(){
        FileInputStream fileIn = null;
        try {
            fileIn = new FileInputStream("LastGameF");
            ObjectInputStream in = null;
            in = new ObjectInputStream(fileIn);
            return (Field) in.readObject();
        } catch (FileNotFoundException e) {
            Field f = new Field();
            return f;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Betölti egy fájlból a mentett játék pályájának eltolódását.
     * @return Visszaadja az eltolódást.
     */
    public static int[] loadVH(){
        FileInputStream fileIn = null;
        try {
            fileIn = new FileInputStream("LastGameVH");
            ObjectInputStream in = null;
            in = new ObjectInputStream(fileIn);
            return (int[]) in.readObject();
        } catch (FileNotFoundException e) {
            int[] VH = new int[4];
            for(int i = 0; i < 4; i++){
                VH[i] = 0;
            }
            return VH;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Betölti melyik bot ellen játszottunk az elmentett játékban.
     * @return Visszaadja a botot nevét.
     */
    public static String loadWhich(){
        FileInputStream fileIn = null;
        try {
            fileIn = new FileInputStream("LastGameWhich");
            ObjectInputStream in = null;
            in = new ObjectInputStream(fileIn);
            return (String) in.readObject();
        } catch (FileNotFoundException e) {
            return "Easy";
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
