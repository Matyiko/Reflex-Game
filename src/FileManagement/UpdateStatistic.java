package FileManagement;

import java.io.*;

/**
 * Ez az osztály frissíti a statisztikát.
 */
public class UpdateStatistic {
    /**
     * Egy fájlba kiírja a jelenlegi statisztikát.
     * @param stats A statisztikát tartalmazó tömb.
     */
    public static void updateStatistics(int[] stats) {
        FileOutputStream fileOut;
        try {
            fileOut = new FileOutputStream("Statistics");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(stats);
            fileOut.close();
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *  Egy fájlból beolvassa a jelenlegi statisztikát, ha nincs még statisztika, akkor létrehoz egyet.
     * @return Visszaad egy a statisztikát tartalmazó tömböt.
     */
    public static int[] loadStatistics(){
        FileInputStream fileIn;
        try {
            fileIn = new FileInputStream("Statistics");
            ObjectInputStream in;
            in = new ObjectInputStream(fileIn);
            return (int[]) in.readObject();
        } catch (FileNotFoundException e) {
            int[] statistics = new int[3];
            for(int i = 0; i < 3; i++){
                statistics[i] = 0;
            }
            return statistics;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}

