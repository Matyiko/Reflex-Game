package FileManagement;

import Game.Field;
import Graphics.Map;

import java.awt.event.ActionListener;
import java.io.*;

/**
 * Ez az osztály menti el a jelenlegi játék állását fájlokba.
 */
public class Save {

    /**
     * Elmenti az adott játék pályáját, botját és a pálya eltolódását fájlokba.
     * @param f A Field.
     * @param map A Map.
     * @param Which Az adott bot.
     */
    public static void save(Field f, Map map, String Which){
        FileOutputStream fileOutF, fileOutMap, fileOutWhich;
        try {
            fileOutF = new FileOutputStream("LastGameF");
            ObjectOutputStream outF = new ObjectOutputStream(fileOutF);
            outF.writeObject(f);
            fileOutF.close();
            outF.close();
            fileOutMap = new FileOutputStream("LastGameVH");
            ObjectOutputStream outMap = new ObjectOutputStream(fileOutMap);
            outMap.writeObject(map.getVerticalHorizontal());
            outMap.close();
            fileOutWhich = new FileOutputStream("LastGameWhich");
            ObjectOutputStream outWhich = new ObjectOutputStream(fileOutWhich);
            outWhich.writeObject(Which);
            outWhich.close();

            System.out.println("Successfully Saved!");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

